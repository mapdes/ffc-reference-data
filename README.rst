------------------
FFC reference data
------------------

This repository contains data used by the regression tests for the
FEniCS Form Compiler (FFC). The reference data are stored in a
separate repository to minimizee the size of the main FFC repository.

Downloading and uploading data from/to this repository is automatically
handled by the scripts scripts/download and scripts/upload which are to
be run from the FFC repository under tests/regression/.

Which commit from this repository to checkout before running FFC
regression tests is stored in a file ffc-reference-data-id.

